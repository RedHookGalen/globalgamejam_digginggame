﻿using UnityEngine;

namespace Assets.Code.GameObjectUtils
{
    public class Lava : MonoBehaviour
    {
        public Vector3 velocity = Vector2.one;

        private void FixedUpdate()
        {
            transform.position += velocity * Time.fixedDeltaTime;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Health health = other.gameObject.GetComponentInChildren<Health>();
            if (health != null)
            {
                health.ForceDeath();
                return;
            }

            Pickup pickup = other.gameObject.GetComponentInChildren<Pickup>();
            if (pickup != null)
            {
                Debug.Log("Crystal lost to the lava. Game Over");
                GameManager.Instance.RunGameOver("Lava Destroyed The Crystal");
            }
        }
    }
}