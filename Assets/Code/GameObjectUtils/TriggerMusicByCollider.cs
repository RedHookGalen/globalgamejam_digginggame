﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMusicByCollider : MonoBehaviour
{
    public AudioClip playClip;
    public bool disableAfterPlay = true;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        //TODO Make this a setting on the trigger
        if(collider.CompareTag("Player") == false)
            return;

        MusicStack.instance.Play(playClip);
        if (disableAfterPlay)
        {
            enabled = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {

    }
}
