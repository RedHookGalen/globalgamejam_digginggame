﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapController : MonoBehaviour
{
    public string weakTileName = "LightGrey_Box64x64";
    public string strongTileName = "DarkGrey_Box64x64";
    public int strengthFactor = 4;

    public Color[] damagedColors = new Color[] {Color.white, Color.white, Color.white, Color.white};

    private int[,] array2d;
    private bool[,] isStrong;

    // Start is called before the first frame update
    void Start()
    {
        Tilemap tilemap = GetComponent<Tilemap>();

        BoundsInt boundsInt = tilemap.cellBounds;

        Vector3Int position = boundsInt.position;
        Vector3Int size = boundsInt.size;

        array2d = new int[size.x, size.y];
        isStrong = new bool[size.x, size.y];

        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                int indx = position.x + i;
                int indy = position.y + j;

                Vector3Int tileCellIndex = new Vector3Int(indx, indy, 0);

                TileBase tileBase = tilemap.GetTile(tileCellIndex);

                if (tileBase)
                {
                    string tilename = tilemap.GetTile(tileCellIndex).name;

                    if (tilename == weakTileName)
                    {
                        array2d[i, j] = 1;
                        isStrong[i, j] = false;
                    } else if (tilename == strongTileName)
                    {
                        array2d[i, j] = strengthFactor;
                        isStrong[i, j] = true;
                    } else
                    {
                        array2d[i, j] = 0;
                        isStrong[i, j] = false;
                    }
                }
            }
        }
        
    }

    public int damageTile(Vector3Int tileCellIndex)
    {
        Tilemap tilemap = GetComponent<Tilemap>();

        BoundsInt boundsInt = tilemap.cellBounds;

        Vector3Int position = boundsInt.position;
        Vector3Int size = boundsInt.size;

        Grid grid = tilemap.layoutGrid;

        int indx = tileCellIndex.x - position.x;
        int indy = tileCellIndex.y - position.y;

        int resource = 0;

        if (array2d[indx, indy] > 0)
        {
            int healthBefore = array2d[indx, indy];
            int healthAfter = healthBefore - 1;

            if (healthAfter == 0)
            {
                tilemap.SetTile(tileCellIndex, null);

                ParticleSystem weakParticleSystem = GameObject.Find("WeakTileParticleSystem").GetComponent<ParticleSystem>();
                ParticleSystem strongParticleSystem = GameObject.Find("StrongTileParticleSystem").GetComponent<ParticleSystem>();

                if (isStrong[indx, indy])
                {
                    resource = 2;
                    strongParticleSystem.transform.position = grid.CellToWorld(tileCellIndex);
                    strongParticleSystem.Play();
                } else
                {
                    resource = 1;
                    weakParticleSystem.transform.position = grid.CellToWorld(tileCellIndex);
                    weakParticleSystem.Play();
                }
            }

            array2d[indx, indy] = healthAfter;

            tilemap.SetTileFlags(tileCellIndex, TileFlags.None);

            Color tileColor = damagedColors[array2d[indx, indy]];

            tilemap.SetColor(tileCellIndex, tileColor);
        }

        return resource;
    }
}
