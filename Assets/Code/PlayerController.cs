﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Code.Commmon;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(PlayerInput))]
public class PlayerController : CharacterBase
{
    public static event Action<PlayerController> OnSpawned;
    public static string LastSceneName { get; private set; }

    public Vector2 raycastPosition;

    [Header("Attack")]
    public float attackCooldown = 1.0f;
    public float attackCastDistace = 2.0f;
    public ContactFilter2D attackContactFilter;
    [Tooltip("How long does the attack raycast happen after the animation starts?")]
    public float attackAnimationDelay = 0.3f;

    [Header("Dig")]
    public Vector2 digDownOrigin = Vector2.zero;
    public Vector2 digDownDir = Vector2.down;
    public Vector2 digUpOrigin = Vector2.up;
    public Vector2 digUpDir = Vector2.up;
    public ContactFilter2D digContactFilter;

    [Header("Carrying")] public Transform m_CarryPosition;
    public float throwForce = 5.0f;

    [Header("Multiplayer")]
    public int playerIndex;
    [NonSerialized]
    public bool isSpawned = false;

    [Header("Damaged")] public Color damageTint = Color.red;
    public float damageFlashTime = 1.0f;

    private PlayerInput _playerInput;
    private InputAction _moveInputAction;
    private InputAction _jumpAction;
    private float _attackCooldownRemaining = 0;
    private Pickup _carriedItem;
    private Color _defaultTint;
    private Coroutine _damageFlashCoroutine;

    private float timeBetweenFootstepSounds = 0.4f;
    private float timeSinceLastIdleSound = 0.0f;

    private PlayerInputManagerController PlayerInputManagerController;
    protected TilemapController tilemapController;

    protected override void Start()
    {
        base.Start();
        _playerInput = GetComponent<PlayerInput>();
        _moveInputAction = _playerInput.actions.FindAction("Move");
        _jumpAction = _playerInput.actions.FindAction("Jump");

        PlayerInputManagerController = FindObjectOfType<PlayerInputManagerController>();
        tilemapController = GameObject.FindGameObjectWithTag("GroundTileMap")?.GetComponent<TilemapController>();

        if (OnSpawned != null)
        {
            OnSpawned.Invoke(this);
        }

        LastSceneName = gameObject.scene.name;

        if (_health != null)
        {
            _health.onDamagedvent.AddListener(OnDamaged);
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        _health.onDamagedvent.RemoveListener(OnDamaged);
        if (GameManager.Instance != null)
        {
            timeBetweenFootstepSounds = GameManager.Instance.timeBetweenFootstepSounds;
        }
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
    private void Init()
    {
        OnSpawned = null;
    }

    protected override void Update()
    {
        base.Update();
        _attackCooldownRemaining = Mathf.Max(0, _attackCooldownRemaining - Time.deltaTime);
    }

    protected override bool CheckIsJumping()
    {
        return base.CheckIsJumping() && _jumpAction.ReadValue<float>() > 0.1f;
    }

    protected override Vector2 GetDesiredMovement()
    {
        Vector2 movement = _moveInputAction.ReadValue<Vector2>();
        timeSinceLastIdleSound += Time.deltaTime;

        if (timeSinceLastIdleSound > timeBetweenFootstepSounds && movement.magnitude != 0)
        {
            switch (UnityEngine.Random.Range(0, 6))
            {
                case 0:
                    GameManager.Instance.PlaySound("Footstep1");
                    break;
                case 1:
                    GameManager.Instance.PlaySound("Footstep2");
                    break;
                case 2:
                    GameManager.Instance.PlaySound("Footstep3");
                    break;
                case 3:
                    GameManager.Instance.PlaySound("Footstep4");
                    break;
                case 4:
                    GameManager.Instance.PlaySound("Footstep5");
                    break;
                case 5:
                    GameManager.Instance.PlaySound("Footstep6");
                    break;
                default:
                    Debug.Log("What?");
                    break;
            }
            timeSinceLastIdleSound = 0;
        }

        return movement;
    }

    public void OnAttack()
    {
        Vector2 moveInput = _moveInputAction.ReadValue<Vector2>();
        if (Mathf.Abs(moveInput.y) > 0.5f)
        {
            OnDig(moveInput.y > 0);
            return;
        }

        if (IsDead() || _attackCooldownRemaining > 0)
            return;

        _animator.SetInteger("AttackVariation", 1);
        _animator.SetTrigger("IsAttacking");

        GameManager.Instance.PlaySound("PlayerHit");

        StartCoroutine(AttackDelay());
    }

    private void OnDamaged()
    {
        if(_damageFlashCoroutine != null)
            StopCoroutine(_damageFlashCoroutine);
        else
        {
            _defaultTint = characterSprite.color;
        }
        _damageFlashCoroutine = StartCoroutine(FlashDamage());
    }

    private IEnumerator FlashDamage()
    {
        float timeRemaining = damageFlashTime * 0.5f;
        while (timeRemaining > 0)
        {
            yield return null;
            timeRemaining -= Time.deltaTime;
            characterSprite.color = Color.Lerp(_defaultTint, damageTint, timeRemaining);
        }
        timeRemaining = damageFlashTime * 0.5f;
        while (timeRemaining < 0)
        {
            yield return null;
            timeRemaining -= Time.deltaTime;
            characterSprite.color = Color.Lerp(damageTint, _defaultTint, timeRemaining);
        }
        characterSprite.color = _defaultTint;
    }

    private IEnumerator AttackDelay()
    {
        yield return new WaitForSeconds(attackAnimationDelay);

        if (_carriedItem != null)
        {
            _carriedItem.transform.SetParent(null);
            _carriedItem.Throw(new Vector2(isFlipped ? -1 : 1, 0.4f));
            _carriedItem = null;
            SetAnimatorBool("IsCarrying", false);
            yield break;
        }

        List<RaycastHit2D> hits = RaycastAttack(new Vector2(0, -0.25f));
        hits.AddRange(RaycastAttack(new Vector2(0, 0.25f)));

        if (hits.Count > 0)
        {            
            foreach (RaycastHit2D hit in hits)
            {
                IAttackReceiver attackReceiver = hit.transform.gameObject.GetComponentInChildren<IAttackReceiver>();
                if (attackReceiver != null)
                {
                    switch (attackReceiver)
                    {
                        case Health h:
                            h.TakeDamage(attackDamage);
                            break;
                        case Pickup p:
                            _carriedItem = p;
                            p.transform.SetParent(m_CarryPosition, false);
                            p.transform.localPosition = Vector3.zero;
                            p.transform.localRotation = Quaternion.identity;
                            p.OnPickup(this);
                            SetAnimatorBool("IsCarrying", true);
                            break;
                        default:
                            break;
                    }
                    _attackCooldownRemaining = attackCooldown;
                    break;
                }
                else
                {
                    Tilemap tilemap = hit.transform.gameObject.GetComponent<Tilemap>();
                    if (tilemap != null)
                    {
                        Dig(tilemap, hit, new Vector3Int[]
                        {
                            new Vector3Int(0, 0, 0)
                        });
                        break;
                    }
                }

            }
        }
    }

    List<RaycastHit2D> RaycastAttack(Vector2 offset)
    {
        List <RaycastHit2D> hits = new List<RaycastHit2D>();
        Physics2D.Raycast(transform.TransformPoint(raycastPosition + offset), isFlipped ? Vector2.right : Vector2.left,
            attackContactFilter, hits, attackCastDistace);
        return hits;
    }

    void OnDig(bool digUp)
    {
        Vector2 origin = digUp ? digUpOrigin : digDownOrigin;
        Vector2 vec = digUp ? digUpDir : digDownDir;

        _animator.SetInteger("AttackVariation", digUp ? 0 : 2);
        _animator.SetTrigger("IsAttacking");
        if (Physics2D.Linecast(transform.TransformPoint(origin),
                transform.TransformPoint(origin + vec), digContactFilter, _rbHits) > 0)
        {
            Tilemap tilemap = _rbHits[0].collider.gameObject.GetComponent<Tilemap>();
            if(tilemap == null)
                return;

            Dig(tilemap, _rbHits[0], new Vector3Int[] { Vector3Int.zero,
                transform.localScale.x < 0 ? new Vector3Int(1, 0, 0) : new Vector3Int(-1,0,0)});
        }
    }

    void Dig(Tilemap tilemap, RaycastHit2D hit, Vector3Int[] tileDigOffsets)
    {
        Grid grid = tilemap.layoutGrid;

        Vector2 tileHitPoint = hit.point - hit.normal * _hitOffset;
        Vector3Int raycastTileCoordinate = grid.WorldToCell(tileHitPoint);
        foreach (var offset in tileDigOffsets)
        {
            Vector3Int coordinate = raycastTileCoordinate + offset;
            if (tilemap)
            {
                int resourceCount = tilemapController.damageTile(coordinate);

                _inventoryCount += resourceCount;
            }
        }
    }

    public bool isFlipped => transform.localScale.x < 0;

    #region Muliplayer Spawning

    public void SpawnPlayer(int controllerIndex)
    {
        playerIndex = controllerIndex;
        Debug.Log("Player Index: " + controllerIndex);

        //isSpawned is check so player is not added to PlayerList repeatedly on Enable/Disable
        if (!isSpawned)
        {
            PlayerInputManagerController?.SetupPlayer(gameObject, playerIndex);
            isSpawned = true;
        }
    }

    public void OnRespawn()
    {
        //Debug.Log("On Respawn");
        PlayerInputManagerController.EnablePlayer(gameObject);
    }

    #endregion

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawLine(digDownOrigin, digDownOrigin + digDownDir);

        Gizmos.DrawLine(digUpOrigin, digUpOrigin + digUpDir);
    }
}
