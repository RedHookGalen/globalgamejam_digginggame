﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

// Singleton
public class GameManager : MonoBehaviour
{
    public float aggroDistance = 3.0f;
    public float giveUpDistance = 5.0f;
    public float calmSpeed = 2.0f;
    public float rageSpeed = 5.0f;
    public float idleSoundCooldown = 1.0f;
    public float idleSoundDistance = 7.0f;
    public float timeBetweenFootstepSounds = 0.4f;

    public Sound[] sounds;


    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        PlayerController.OnSpawned += OnPlayerSpawned;

        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume = 0.7f;
            s.source.pitch = s.pitch = 1.0f;
        }
    }

    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, sounds => sounds.name == name);
        if (s != null && s.source != null)
        {
            s.source.Play();
        }
    }

    private void OnDestroy()
    {
        _instance = null;
        PlayerController.OnSpawned -= OnPlayerSpawned;
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.F1))
        {
            SceneManager.LoadScene("Victory");
        }

        if (Input.GetKeyUp(KeyCode.F2))
        {
            SceneManager.LoadScene("GameOver");
        }
    }

    private void OnPlayerSpawned(PlayerController player)
    {
        _players.Add(player);
        player.GetComponent<Health>().onDeathEvent.AddListener(OnPlayerDied);
    }

    private void OnPlayerDied()
    {
        foreach (var player in _players)
        {
            if (!player.IsDead())
            {
                return;
            }
        }

        //They are all dead
        RunGameOver("All players died");
    }

    int numberOfEnemies = 0;
    GameObject personWithCrystal = null;
    public int GetNumberOfEnemies() { return numberOfEnemies; }  

    public void EnemySpawned() { numberOfEnemies++; }
    public void EnemyKilled() { numberOfEnemies--; }
    public GameObject GetPersonWithCrystal() { return personWithCrystal; }
    public void SetPersonWithCrystal(GameObject person) { personWithCrystal = person; }

    public delegate void OnCorePickedUp(PlayerController player);
    public static event OnCorePickedUp onCorePickedUp;
    public void RaiseOnCorePickedUp(PlayerController player)
    {
        if (onCorePickedUp != null)
        {
            onCorePickedUp(player);
        }
    }

    public delegate void OnCoreDrop();
    public static event OnCoreDrop onCoreDrop;
    public void RaiseOnCoreDrop()
    {
        if (onCoreDrop != null)
        {
            onCoreDrop();
        }
    }
    public List<PlayerController> _players = new List<PlayerController>();

    public List<PlayerController> AllPlayers => _players;

    public void RunGameOver(string why)
    {
        StartCoroutine(DelayGameOver());
    }

    private IEnumerator DelayGameOver()
    {
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene("GameOver");
    }
}