﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputManagerController : MonoBehaviour
{

    public List<GameObject> playerList = new List<GameObject>();

    //Index of Colors aligns with Device index of Controllers
    //TODO: Create other similar array of items for stats, etc.
    private Color[] playerColors = { Color.cyan, Color.magenta, Color.green, Color.yellow };

    PlayerInputManager playerInputManager;
    public GameObject playerPrefab;
    public int maxPlayers = 4;
    public float randomXSpawn = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        playerInputManager = FindObjectOfType<PlayerInputManager>();
        //Debug.Log("Player Count: " + playerInputManager.playerCount);
        //Debug.Log("Devices Count: " + InputSystem.devices.Count);

        int currentPlayerCount = 0;

        for(int i = InputSystem.devices.Count - 1; i > -1; --i)
        {
            if(currentPlayerCount >= maxPlayers)
                return;

            if(InputSystem.devices[i].description.deviceClass.Contains("Mouse"))
                continue;
            PlayerInput playerInput = PlayerInput.Instantiate(playerPrefab);
            float randomXOffset = Random.Range(-randomXSpawn, randomXSpawn);
            playerInput.transform.position = transform.position + new Vector3(randomXOffset, 0, 0);
            playerInput.SwitchCurrentControlScheme(InputSystem.devices[i]);
            playerInput.neverAutoSwitchControlSchemes = true;
            PlayerController playerController = playerInput.gameObject.GetComponent<PlayerController>();
            playerController.SpawnPlayer(i);
            SetupPlayer(playerController.gameObject, currentPlayerCount);
            ++currentPlayerCount;
            //Instantiate(playerPrefab, this.transform);
        }

        
    }

    // Update is called once per frame
    //TODO: Add code that allows for late controller joining. Check player devices vs all devices
    void Update()
    {


        //Repspawn All
        if (Input.GetKeyDown(KeyCode.Space))
        {
            EnableAllPlayers();
        }
    }
    
     //Setup is run from Player Controller when player is instantiated
    public void SetupPlayer(GameObject player, int playerInputIndex)
    {
        if (!playerList.Contains(player))
        {
            //Debug.Log("Player ID/Index: " + playerInputIndex);
            player.GetComponentInChildren<SpriteRenderer>().color = playerColors[playerInputIndex];
            //TODO: Setup CharacterBase Stats from GetComponent<CharacterBase> here
            playerList.Add(player);
        }
    }
    

    public void DisablePlayer(GameObject playerToDisable)
    {
       foreach(GameObject player in playerList)
        {
            if(player == playerToDisable)
            {
                player.SetActive(false);
            }
        }
    }


    public void EnablePlayer(GameObject playerToEnable)
    {
        foreach (GameObject player in playerList)
        {
            if (player == playerToEnable)
            {
                player.SetActive(true);
            }
        }
    }

    public void EnableAllPlayers()
    {
        foreach (GameObject player in playerList)
        {
            if (!player.activeSelf)
            {
                player.SetActive(true);
            }
        }
    }

    public void DisableAllPlayers()
    {
        foreach (GameObject player in playerList)
        {
         
           player.SetActive(false);
            
        }
    }

}
