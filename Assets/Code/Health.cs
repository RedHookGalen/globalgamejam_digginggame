﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Code.Commmon;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour, IAttackReceiver
{
    public int health = 1;
    public UnityEvent onDeathEvent;
    public UnityEvent onDamagedvent;

    private int maxHealth;

    private void Start()
    {
        maxHealth = health;
    }

    public void TakeDamage(int damage)
    {
        if(IsDead() == true)
            return; //Alread dead

        health = Mathf.Max(0, health - damage);
        if (IsDead())
        {
            Debug.Log($"{name} died");
            onDeathEvent.Invoke();
        }
        else
        {
            onDamagedvent.Invoke();
        }
    }

    public bool IsDead()
    {
        return health == 0;
    }

    [ContextMenu("Force Death")]
    public void ForceDeath()
    {
        TakeDamage(int.MaxValue);
    }
}
