﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float spawnArea = 1.0f;
    public float removalDelay;
    public bool isTriggerable = false;
    public int concurrentEnemiesToBeSpawned;

    // proper realisation
    public int totalEnemiesToBeSpawned;
    public float timeBetweenSpawns;
    public Enemy[] enemyPrefabs;

    GameObject[] spawnerPoints;
    //int numberOfEnemies = 0;
    float timeSinceLastSpawn;

    protected bool isInitiallyTriggered = false;
    protected int m_TotalSpawnedEnemyCount;
    protected int m_CurrentSpawnedEnemyCount;

    void FixedUpdate()
    {
        if (GameManager.Instance.GetNumberOfEnemies() >= totalEnemiesToBeSpawned)
        {
            timeSinceLastSpawn = 0;
            return;
        }

        timeSinceLastSpawn += Time.deltaTime;
        if (timeSinceLastSpawn >= timeBetweenSpawns)
        {
            timeSinceLastSpawn -= timeBetweenSpawns;
            int nextSpawner = Random.Range(0, spawnerPoints.Length);
            if (spawnerPoints[nextSpawner])
                SpawnEnemy(nextSpawner);
        }
    }

    void SpawnEnemy(int nextSpawner)
    {
        Enemy prefab = enemyPrefabs[Random.Range(0, enemyPrefabs.Length)];
        Enemy spawn = Instantiate<Enemy>(prefab);
        spawn.transform.localPosition = spawnerPoints[nextSpawner].transform.position;
        GameManager.Instance.EnemySpawned();
    }

    void Start()
    {
        spawnerPoints = GameObject.FindGameObjectsWithTag("Spawner_Point");

        if (!isTriggerable)
        {
            StartSpawning();
        }
    }

    void StartSpawning()
    {
        // TODO
        isInitiallyTriggered = true;
    }

    void InitiallyTrigger()
    {
        if (!isInitiallyTriggered)
        {
            StartSpawning();
        }
    }
}