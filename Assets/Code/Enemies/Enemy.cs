﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : CharacterBase
{
    
    [Header("Enemy specific")]
    //public float distanceFromPlayerToStop = 1.0f;
    public float bounceThrust = 10f;
    public float timeTillDissapear = 1.0f;    
     

    float aggroDistance = 3.0f;
    bool isRaging = false;
    float timeSinceLastIdleSound = 0;
    float giveUpDistance = 5.0f;
    float idleSoundCooldown = 1.0f;
    float idleSoundDistance = 7.0f;
    Transform target;

    GameManager gameManager;
    private Coroutine m_waitToDissapear = null;

    protected override void Start()
    {
        base.Start();
        gameManager = GameManager.Instance;
        aggroDistance = gameManager.aggroDistance;
        groundSpeed = gameManager.calmSpeed;
        giveUpDistance = gameManager.giveUpDistance;
        idleSoundCooldown = gameManager.idleSoundCooldown;
        idleSoundDistance = gameManager.idleSoundDistance;

        bool isRaging = false;
        GameManager.onCorePickedUp += SetRage;
        GameManager.onCoreDrop += SetCalm;

        /*foreach (GameObject player in players)
        {
            if (!target)
                target = player.transform;
            else
            {
                if (Vector2.Distance(player.transform.position, transform.position) < 
                    Vector2.Distance(target.position, transform.position))
                {
                    target = player.transform;
                }
            }
        }*/
    }

    protected override void OnDestroy()
    {
        GameManager.onCorePickedUp -= SetRage;
        GameManager.onCoreDrop -= SetCalm;
    }

    protected override Vector2 GetDesiredMovement()
    {
        if (!target)
        {
            FindNewTarget();
        }

        if (!target)
        {
            _animator.SetFloat("Speed", 0);
            return Vector2.zero;
        }
        float dist = Vector2.Distance(target.position, transform.position);
        if (dist > aggroDistance && dist > giveUpDistance)
        {
            _animator.SetFloat("Speed", 0);
            target = null;
            gameManager.PlaySound("Hunger");
            return Vector2.zero;
        }

        Vector2 heading = target.position - transform.position;
        float distance = heading.magnitude;
        _animator.SetFloat("Speed", groundSpeed);

        timeSinceLastIdleSound += Time.deltaTime;
        if (timeSinceLastIdleSound > idleSoundCooldown)
        {
            gameManager.PlaySound("Zombie4");
            timeSinceLastIdleSound = 0;
        }


        return heading / distance;
    }

    void FindNewTarget()
    {
        foreach (PlayerController player in GameManager.Instance.AllPlayers)        
            if (Vector2.Distance(player.transform.position, transform.position) <= idleSoundDistance)
            {
                timeSinceLastIdleSound += Time.deltaTime;
                
                if (Vector2.Distance(player.transform.position, transform.position) <= aggroDistance)
                {
                    if (player.GetComponentInChildren<Health>().health > 0)
                        target = player.transform;
                }
                else if (timeSinceLastIdleSound > idleSoundCooldown)
                {
                    switch (Random.Range(0, 4))
                    {
                        case 0:
                            gameManager.PlaySound("Zombie3");
                            break;
                        case 1:
                            gameManager.PlaySound("Zombie4");
                            break;
                        case 2:
                            gameManager.PlaySound("Zombie5");
                            break;
                        case 3:
                            gameManager.PlaySound("Crystal1");
                            //Debug.Log("Playing Crystal1");
                            break;
                        default:
                            Debug.Log("What?");
                            break;
                    }
                    timeSinceLastIdleSound = 0;
                }
            }
    }

    void SetRage(PlayerController player)
    {
        aggroDistance = 10000;
        groundSpeed = gameManager.rageSpeed;
        target = player.transform;
        bool isRaging = true;
        gameManager.PlaySound("Crystal3");

    }

    void SetCalm()
    {
        aggroDistance = gameManager.aggroDistance;
        groundSpeed = gameManager.calmSpeed;
        target = null;
        bool isRaging = false;
        FindNewTarget();
    }

    void Bounce()
    {
        Vector2 heading = target.position - transform.position;
        float distance = heading.magnitude;
        Vector2 direction = heading / distance;
        _rigidbody2D.AddForce((-direction) * bounceThrust, ForceMode2D.Impulse);
        
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (_health.health <= 0)
            return;
        if (col.gameObject.tag == "Player")
        {
            Health health = col.gameObject.GetComponentInChildren<Health>();
            Debug.Log("OnCollisionEnter2D with player, health left: "+ health.health);
            if (health != null)
            {
                if (health.health <= 0)
                {
                    target = null;
                    gameManager.PlaySound("Crystal2");
                    timeSinceLastIdleSound = 0;
                    return;
                }
                health.TakeDamage(attackDamage);
                if (Random.Range(0, 2) == 0)
                {
                    gameManager.PlaySound("EnemyHitPlayer1");
                }
                else
                    gameManager.PlaySound("EnemyHitPlayer2");
                _isBouncing = true;
                Bounce();
            }
        }
    }

    override protected void SetAnimatorBool(string name, bool value)
    {
        if (_animator == null || _animator.runtimeAnimatorController == null)
            return;

        //Debug.Log("state: " + name);

        //Debug.Log("state: " + name);
        //_animator.SetFloat("Speed", groundSpeed);

        //switch (name)
        //{
        //    case "isWalking":
        //        Debug.Log("state: " + name);
        //        _animator.SetFloat("Speed", groundSpeed);
        //        break;
        //    case "isJumping":
        //        _animator.SetFloat("Speed", groundSpeed);
        //        break;
        //    case "isDead":
        //        _animator.SetBool(name, value);
        //        break;
        //    default:
        //        //Console.WriteLine("Default case");
        //        break;
        //}

        //if (name == "isWalking")
        //    _animator.SetFloat("Speed", groundSpeed);

        //_animator.SetBool(name, value);
    }

    protected override bool CheckIsJumping()
    {
        //Todo: Give the enemies jump logic
        return false;
    }

    protected override void OnDeath()
    {
        Debug.Log("Enemy is dead");
        _animator.SetBool("isDead", true);
        _rigidbody2D.simulated = false;
        enabled = false;
        gameManager.PlaySound("EnemyDeath");
        //m_waitToDissapear = StartCoroutine(WaitToDissapear());

        //StopCoroutine(m_waitToDissapear);
        //m_waitToDissapear = null;
    }

    private IEnumerator WaitToDissapear()
    {
        yield return new WaitForSeconds(timeTillDissapear);
    }
}
