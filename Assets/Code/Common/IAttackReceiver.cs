﻿using UnityEngine;

namespace Assets.Code.Commmon
{
    public interface IAttackReceiver
    {
        GameObject gameObject { get; }
    }
}