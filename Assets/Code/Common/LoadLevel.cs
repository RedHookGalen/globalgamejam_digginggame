﻿using UnityEngine;
using UnityEngine.InputSystem.Interactions;
using UnityEngine.SceneManagement;

namespace Assets.Code.Commmon
{
    public class LoadLevel : MonoBehaviour
    {
        public void LoadThisLevel(string sceneName)
        {   
            SceneManager.LoadScene(sceneName);
        }

        public void LoadLastLevel()
        {
            if (string.IsNullOrEmpty(PlayerController.LastSceneName))
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                SceneManager.LoadScene(PlayerController.LastSceneName);
            }
        }
    }
}