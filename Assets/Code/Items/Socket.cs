﻿using UnityEngine;
using UnityEngine.Events;

public class Socket : MonoBehaviour
{
    public Transform itemHolder;
    public UnityEvent onSocketed;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Pickup pickup = other.gameObject.GetComponent<Pickup>();
        if(pickup == null)
            return;

        pickup.OnSocketed();
        pickup.transform.SetParent(itemHolder, false);
        pickup.transform.localPosition = Vector3.zero;
        pickup.transform.localRotation = Quaternion.identity;

        Debug.Log("Socketted!");
        onSocketed.Invoke();
    }

    private void OnTriggerExit2D(Collider2D collider)
    {

    }
}