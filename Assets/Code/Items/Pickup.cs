﻿using System.Collections;
using System.Collections.Generic;
using Assets.Code.Commmon;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public class Pickup : MonoBehaviour, IAttackReceiver
{
    public float throwForce = 10.0f;
    public float timeTillFloat = 1.0f;
    public ContactFilter2D floatContactFilter;
    public AudioClip pickUpAudio;
    public AudioClip theme;
    public float themeDelay = 5.0f;
    public UnityEvent onFirstPickup;

    private Rigidbody2D m_rigidBody;
    private Animator m_animator;
    private List<RaycastHit2D> m_groundResults = new List<RaycastHit2D>();
    private Coroutine m_waitToFloatRoutine = null;
    private bool m_firstPickup = true;
    private PlayerController m_holder;

    private void Start()
    {
        m_animator = GetComponent<Animator>();
        m_rigidBody = GetComponent<Rigidbody2D>();
        m_rigidBody.simulated = true;
        m_rigidBody.isKinematic = true;
    }

    private void Update()
    {
        if (m_holder != null && m_holder.IsDead())
        {
            Throw(Vector2.down);
        }
    }

    public void OnPickup(PlayerController player)
    {
        m_holder = player;

        if (m_waitToFloatRoutine != null)
        {
            StopCoroutine(m_waitToFloatRoutine);
            m_waitToFloatRoutine = null;
        }
        Debug.Log($"{name} has been picked up");
        m_animator.SetBool("IsCarried", true);
        m_rigidBody.isKinematic = true;
        m_rigidBody.simulated = false;
        GameManager.Instance.RaiseOnCorePickedUp(player);

        if (m_firstPickup)
        {
            StartCoroutine(FirstPickupAudioRoutine());
            onFirstPickup.Invoke();
            m_firstPickup = false;
        }
    }

    public void Throw(Vector2 throwDirection)
    {
        m_holder = null;

        m_rigidBody.isKinematic = false;
        m_rigidBody.simulated = true;
        m_rigidBody.AddForce(throwDirection * throwForce, ForceMode2D.Impulse);
        m_animator.SetBool("IsCarried", false);

        GameManager.Instance.RaiseOnCoreDrop();
        m_waitToFloatRoutine = StartCoroutine(WaitToFloat());
    }

    private IEnumerator WaitToFloat()
    {
        yield return new WaitForSeconds(timeTillFloat);

        while (m_rigidBody.Cast(Vector2.down, floatContactFilter, m_groundResults, 0.2f) == 0)
        {
            yield return null;
        }

        m_rigidBody.isKinematic = true;
        m_rigidBody.velocity = Vector2.zero;
        m_rigidBody.angularVelocity = 0;
    }

    public void OnSocketed()
    {
        m_rigidBody.simulated = false;
    }

    private IEnumerator FirstPickupAudioRoutine()
    {
        MusicStack.instance.Play(pickUpAudio);
        yield return new WaitForSeconds(themeDelay);
        MusicStack.instance.Play(theme);
    }
}
