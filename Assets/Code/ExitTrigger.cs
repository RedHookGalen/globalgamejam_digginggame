﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ExitTrigger : MonoBehaviour
{
    private List<PlayerController> containedPlayers = new List<PlayerController>();
    public UnityEvent onExit;
    private bool exited = false;

    public float timeBeforeUnloading = 10.0f;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var playerController = other.gameObject.GetComponent<PlayerController>();
        if (playerController != null)
        {
            containedPlayers.Add(playerController);

            PlayerController[] allPlayers = GameObject.FindObjectsOfType<PlayerController>();
            foreach (var player in allPlayers)
            {
                if (!player.IsDead() && !containedPlayers.Contains(player))
                {
                    return;
                }
            }

            //Freeze everybody in the exit zone
            foreach (var exitingPlayer in containedPlayers)
            {
                exitingPlayer.enabled = false;
                exitingPlayer.transform.SetParent(transform, true);
                exitingPlayer.GetComponent<Rigidbody2D>().simulated = false;
            }

            //We are all here let's go!
            Debug.Log("Exit level");
            DisablePlayerSplitScreenCameras();
            onExit.Invoke();

            StartCoroutine(WaitToExit());
        }
    }

    private void DisablePlayerSplitScreenCameras()
    {
        Camera[] playerCams = FindObjectsOfType<Camera>();

        foreach(Camera cam in playerCams)
        {
            if (cam.CompareTag("PlayerSplitScreenCamera"))
            {
                cam.gameObject.SetActive(false);
            }
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var playerController = other.gameObject.GetComponent<PlayerController>();
        if (playerController != null)
        {
            containedPlayers.Remove(playerController);
        }
    }

    private IEnumerator WaitToExit()
    {
        yield return new WaitForSeconds(timeBeforeUnloading);

        SceneManager.LoadScene("Victory");
    }
}
