﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicStack : MonoBehaviour
{
    public static MusicStack instance;

    public List<AudioClip> clips = new List<AudioClip>();
    public GameObject audioSourceInstance;
    public float fadeTime = 2.0f;

    private Dictionary<AudioClip, AudioSource> m_ClipToSourceDictionary = new Dictionary<AudioClip, AudioSource>();
    private AudioSource m_CurrentMusic;
    private List<AudioSource> m_FadingOut = new List<AudioSource>();

    private void Start()
    {
        instance = this;
        audioSourceInstance.SetActive(false);

        bool first = true;
        foreach (AudioClip clip in clips)
        {
            AudioSource source = SpawnSource(clip, first ? 1.0f : 0.0f);
            if (first)
            {
                m_CurrentMusic = source;
            }
            first = false;
        }
    }

    private void OnDestroy()
    {
        instance = null;
    }

    private void Update()
    {
        if (m_CurrentMusic.volume < 1.0f)
        {
            m_CurrentMusic.volume += Time.deltaTime / fadeTime;
            m_CurrentMusic.volume = Mathf.Max(0, m_CurrentMusic.volume);
        }

        for (int i = m_FadingOut.Count - 1; i > -1; --i)
        {
            m_FadingOut[i].volume -= Time.deltaTime / fadeTime;
            if (m_FadingOut[i].volume <= 0)
            {
                m_FadingOut[i].volume = 0;
                m_FadingOut.RemoveAt(i);
            }
        }
    }

    private AudioSource SpawnSource(AudioClip clip, float volume = 1.0f)
    {
        GameObject go = GameObject.Instantiate(audioSourceInstance, transform);
        go.SetActive(true);
        AudioSource source = go.GetComponent<AudioSource>();
        source.clip = clip;
        source.volume = volume;
        m_ClipToSourceDictionary.Add(clip, source);
        source.Play();
        source.name = clip.name;
        return source;
    }

    public void Play(AudioClip clip)
    {
        if(clip == m_CurrentMusic)
            return; //We are already playing it

        if (clip == null)
        {
            Debug.LogError("No clip passed in");
            return;
        }

        if (!m_ClipToSourceDictionary.ContainsKey(clip))
        {
            Debug.LogWarning($"{clip.name} Audio Clip should be in clips list, might be offset");
            clips.Add(clip);
            SpawnSource(clip, 0.0f);
        }

        m_FadingOut.Add(m_CurrentMusic);
        m_CurrentMusic = m_ClipToSourceDictionary[clip];
        if (m_FadingOut.Contains(m_CurrentMusic))
        {
            m_FadingOut.Remove(m_CurrentMusic);
        }
    }

    [ContextMenu("Next Track")]
    public void NextTrack()
    {
        int clipIndex = clips.IndexOf(m_CurrentMusic.clip);
        ++clipIndex;
        if (clipIndex >= clips.Count)
        {
            clipIndex = 0;
        }
        Play(clips[clipIndex]);
    }
}
