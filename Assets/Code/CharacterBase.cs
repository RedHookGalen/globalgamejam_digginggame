﻿using System.Collections;
using System.Collections.Generic;
using Assets.Code.Commmon;
using UnityEngine;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public abstract class CharacterBase : MonoBehaviour
{
    public int attackDamage = 1;

    [Header("Movement")]
    public float groundSpeed = 5.0f;
    public float jumpSpeed = 10.0f;
    public float groundDistanceCheck = 0.1f;
    public float jumpForceMinTime = 0.1f;
    public float jumpForceMaxTime = 0.3f;
    public ContactFilter2D groundContactFilter;
    public float climbSpeed = 10.0f;
    public int ladderSize = 8;

    [Header("Visuals")]
    public SpriteRenderer characterSprite;

    public Tile ladderTile;

    protected Health _health;
    protected Rigidbody2D _rigidbody2D;
    protected Animator _animator;
    protected List<RaycastHit2D> _rbHits = new List<RaycastHit2D>();
    protected float _jumpHeldTime = 0;
    protected bool _isOnGround = false;
    protected bool _isOnLadder = false;
    protected bool _hasClimbed = false;
    protected bool _isJumping = false;
    protected bool _flippedSprite = false;
    protected bool _canJump = false;
    protected bool _isFacingRight = false;
    protected float _hitOffset = 0.01f;
    protected bool _isBouncing = false;
    protected bool _canClimb = false;
    protected int _inventoryCount = 0;
    protected float _airTime = 0;

    protected virtual void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _health = GetComponentInChildren<Health>();
        if (_health != null)
        {
            _health.onDeathEvent.AddListener(OnDeath);
        }
    }

    protected virtual void OnDestroy()
    {
        if (_health != null)
        {
            _health.onDeathEvent.RemoveListener(OnDeath);
        }
    }

    protected virtual void Update()
    {
        if(IsDead())
            return;

        if (_isOnGround || _hasClimbed)
        {
            _jumpHeldTime = 0;
            _canJump = true;
        }

        _isJumping = CheckIsJumping();

        if (_isJumping)
        {
            _jumpHeldTime += Time.deltaTime;
        }

        SetAnimatorBool("IsJumping", _airTime > 0.1f);

        if (!_isJumping)
        {
            if (_jumpHeldTime > 0.0f && _jumpHeldTime < jumpForceMinTime)
            {
                _isJumping = true;
                _jumpHeldTime += Time.deltaTime;
            }
            else
            {
                _canJump = false;
            }
        }
    }

    protected virtual bool CheckIsJumping()
    {
        if (IsDead())
            return false;

        return _canJump && _jumpHeldTime < jumpForceMaxTime;
    }

    protected virtual Vector2 GetDesiredMovement()
    {
        return Vector2.zero;
    }

    protected virtual void OnDeath()
    {
        SetAnimatorBool("IsDead", true);
    }

    public bool IsDead()
    {
        if (_health == null)
            return false;

        return _health.IsDead();
    }

    void FixedUpdate()
    {
        if (_isBouncing)
        {
            _isBouncing = false;
            return;
        }
        int groundHits = _rigidbody2D.Cast(Vector2.down, groundContactFilter, _rbHits, groundDistanceCheck);

        Vector2 moveInput = GetDesiredMovement();

        if(IsDead())
            moveInput = Vector2.zero;

        Vector2 movement = Vector2.zero;
        movement += new Vector2(moveInput.x * (groundSpeed), 0);

        if (groundHits == 0)
        {
            _isOnGround = false;
        } else
        {
            _isOnGround = true;
            _hasClimbed = false;
        }

        if (_isOnGround || _hasClimbed)
        {
            _airTime = 0;
        } else
        {
            _airTime += Time.deltaTime;
        }

        _isOnLadder = checkIsOnLadder();

        if (_isOnLadder && moveInput.y != 0)
        {
            _hasClimbed = true;
            movement += new Vector2(0, moveInput.y * (climbSpeed));
        }
        else if (_hasClimbed && !_isOnGround && !_isJumping && moveInput.y < 0)
        {
            movement += new Vector2(0, moveInput.y * (climbSpeed));
        }

        if (!_isOnGround && !_hasClimbed)
        {
            movement += (Vector2)Physics.gravity;
        }

        if (_isJumping)
        {
            _hasClimbed = false;
            movement += Vector2.up * jumpSpeed;
        }

        if (_hasClimbed && !_isOnGround)
        {
            movement = new Vector2(0, movement.y);
        }

        SetAnimatorBool("IsWalking", (Mathf.Abs(moveInput.x) > 0.3f && _airTime < 0.1f) || (_hasClimbed && Mathf.Abs(moveInput.y) > 0.0f));

        if (Mathf.Abs(moveInput.x) >= 0.1f)
        {
            _isFacingRight = moveInput.x > 0;
            Vector3 currentScale = transform.localScale;
            currentScale.x = Mathf.Abs(currentScale.x) * (_isFacingRight ? -1 : 1);
            transform.localScale = new Vector3(_isFacingRight ? -1 : 1, 1, 1);
        }
        _rigidbody2D.MovePosition(_rigidbody2D.position + movement * Time.fixedDeltaTime);
    }

    virtual protected void SetAnimatorBool(string name, bool value)
    {
        if(_animator == null || _animator.runtimeAnimatorController == null)
            return;

        _animator.SetBool(name, value);
    }

    void OnLadder()
    {
        Vector2 position = _rigidbody2D.position;

        Tilemap tilemap = GameObject.Find("LadderTileMap")?.GetComponent<Tilemap>();

        if (tilemap)
        {
            Grid grid = tilemap.layoutGrid;

            Vector3Int tileCellCoord = grid.WorldToCell(position);

            if (_inventoryCount > ladderSize)
            {
                for (int i = 0; i < ladderSize; i++)
                {
                    tilemap.SetTile(tileCellCoord + new Vector3Int(0, i, 0), ladderTile);
                }

                _inventoryCount -= ladderSize;
            }
            
        }
    }

    bool checkIsOnLadder()
    {
        Vector2 position = _rigidbody2D.position;

        GameObject tilemapObject = GameObject.Find("LadderTileMap");

        if (tilemapObject)
        {
            Tilemap tilemap = GameObject.Find("LadderTileMap").GetComponent<Tilemap>();

            if (tilemap)
            {
                Grid grid = tilemap.layoutGrid;

                Vector3Int tileCellCoord = grid.WorldToCell(position);

                TileBase tileBase = tilemap.GetTile(tileCellCoord);


                return tileBase != null;
            }
        }

        return false;
    }
}
